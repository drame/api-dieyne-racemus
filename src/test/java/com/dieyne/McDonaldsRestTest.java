package com.dieyne;

import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.hamcrest.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.SpringBootMockMvcBuilderCustomizer;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.web.SpringBootMockServletContext;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.dieyne.interfaces.McDonaldsInterface;
import com.dieyne.rest.McDonaldsRest;
import com.dieyne.services.MacDonaldsService;

@RunWith(SpringJUnit4ClassRunner.class)
@WebMvcTest(controllers = McDonaldsRest.class)
public class McDonaldsRestTest {
	 @Autowired
	  private MockMvc mockMvc;
	
	 
	  @Before
	  public void setUp() throws Exception{
		  //mockMvc=MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
		  
	  }
	  @Test
	  public void testMcDonalds() throws Exception {
	        mockMvc.perform(get("/mcdonalds").param("code", "ak"))
	                .andExpect(status().isOk())
	                .andDo(print())
	                .andExpect(jsonPath("$", Matchers.hasSize(0)));
	    }
	  @TestConfiguration
	  protected static class Config {
	 
	    @Bean
	    public MacDonaldsService service() {
	      return Mockito.mock(MacDonaldsService.class);
	    }
	 
	  }
}
