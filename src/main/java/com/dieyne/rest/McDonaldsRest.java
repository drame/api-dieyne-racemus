package com.dieyne.rest;

import java.util.List;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.dieyne.entities.MacDonalds;
import com.dieyne.interfaces.McDonaldsInterface;

@RestController
@CrossOrigin("*")
public class McDonaldsRest {

private McDonaldsInterface mcInt;

public  McDonaldsRest(McDonaldsInterface mcInt) {
	// TODO Auto-generated constructor stub
	this.mcInt=mcInt;
}
@GetMapping(value="/mcdonalds",produces = MediaType.APPLICATION_JSON_VALUE)
	public  @ResponseBody List<MacDonalds> findByCode(@RequestParam("code") String code){
		return mcInt.findByCode(code);
	}
}
