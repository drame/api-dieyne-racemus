package com.dieyne.interfaces;

import java.util.List;

import com.dieyne.entities.MacDonalds;

public interface McDonaldsInterface {
public List<MacDonalds> findByCode(String code);
}
