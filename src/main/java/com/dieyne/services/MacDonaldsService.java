package com.dieyne.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.annotation.PostConstruct;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import com.dieyne.entities.MacDonalds;
import com.dieyne.interfaces.McDonaldsInterface;
@Service
public class MacDonaldsService implements McDonaldsInterface{
	private static final Logger LOGGER = LoggerFactory.getLogger(MacDonaldsService.class.getName());
private List<MacDonalds> list= new ArrayList<>();
public  MacDonaldsService() {
	// TODO Auto-generated constructor stub
}
@PostConstruct
public void initList() {
    try {
    	 File file = ResourceUtils.getFile("classpath:mcdonalds.csv");
    	 InputStream in = new FileInputStream(file);
        Reader r = new InputStreamReader(in);
        Iterable<CSVRecord> records = CSVFormat.DEFAULT
        	      .parse(r);
        	    for (CSVRecord record : records) {
        	        String macString = record.get(2);
        	        int index=macString.lastIndexOf(",");
        	        String[] arr=macString.split(",");
        	        MacDonalds mcdonald= new MacDonalds(macString.substring(index+1), macString.substring(0, index));
        	        list.add(mcdonald);
        	    }
    } catch (IOException e) {
    	LOGGER.error(e.getMessage());
    }	
}

@Override
public List<MacDonalds> findByCode(String code) {
	// TODO Auto-generated method stub
	List<MacDonalds> findElements= new ArrayList<>();
	if(list.size()>0&&code.length()>0) {
		for(MacDonalds mc:list) {
			if(mc.getCode().toLowerCase().indexOf(code.toLowerCase())!=-1) {
				findElements.add(mc);
			}
		}
		
	}
	System.out.println("size"+findElements.size());
	return findElements	;
}


}
